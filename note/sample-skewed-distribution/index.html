<!DOCTYPE html>
<html lang="en-gb">
    <head>
        <meta charset="UTF-8">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Static site using Zola">
        <meta name="author" content="galileilei">
        <title></title>
        <link rel="stylesheet" href="https://galileilei.bitbucket.io/site.css"/>
        
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
  </script>
  <script type="text/javascript"
  src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

    </head>
    <body>

        <header>
            <nav class="container">
                <a class="white" href="https://galileilei.bitbucket.io/about/" class="nav-link">About</a>
                <a class="white" href="https://galileilei.bitbucket.io/resume/" class="nav-link">Resume</a>
                <a class="white" href="https://galileilei.bitbucket.io/note/" class="nav-link">Notes</a>
                <a class="white" href="https://galileilei.bitbucket.io/projects/" class="nav-link">Projects</a>
            </nav>
        </header>

        <div class="content  content--reversed">
            
<div class="documentation__content">
<h1 class="title">
  Sampling A Skewed Distribution
</h1>
<p class="subtitle"><strong>2023-04-21</strong></p>
  <h2 id="motivation">Motivation</h2>
<p>To answer this question:</p>
<p>How to sample from a distribution given its mean $mu$, variance $\sigma^2$ and skewness $s$?</p>
<p>When skewness is zero, the problem reduces to sampling from normal distribution. Surely it is easy to extend it for $s \neq 0$, right?
The answer from chatGPT looked promising, but failed to simulation for skewness outside of [-1, 1].</p>
<p>Several iteration later, the question becomes:</p>
<p>What is the maximum entropy distribution, given its first $k$ moments?</p>
<p>Much of this post is from the paper <a href="https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1730203">Entropy Densities</a> with an implementation in Python at the end.</p>
<h2 id="exact-solution">Exact Solution</h2>
<p>It turns out that there is an exact solution to the above problem:</p>
<p>$$p \in argmax - \int_{x \in D} p(x) \log(p(x)) dx$$</p>
<p>where $D$ is some convex domain, $\int_{x \in D} p(x) dx = 1$ and $\int_{x \in D} x^i p(x) dx = b_i$ for $i = 1, \ldots, m$.</p>
<p>Claim 1: The solution has the form $p(x) = \frac{1}{Q(\lambda)} \exp(\sum_{i=1}^m \lambda_i (x^i - b_i))$ for some number $\lambda = (\lambda_1, \ldots, \lambda_m) \in \mathbb{R}^m$, where the normalization constant is $Q(\lambda) = \int_D \exp(\sum_{i =1}^m (x^i - b_i)) dx$.</p>
<p>Proof: see equation (8) from the paper.</p>
<p>Claim 2: The minimization of $Q(\lambda)$ yields a density satisfying the moment constraints.</p>
<p>Proof: see page 5 from above paper.</p>
<p>If only the first moment is specified, we have an exponential distribution.</p>
<p>If only the first two moments are specified, we have a normal distribution.</p>
<p>In all other cases, there is no closed form expression; now we turn to numeric approximation, exploiting Claim 2.</p>
<h2 id="numerical-approximation">Numerical Approximation</h2>
<p>What is left are:</p>
<ol>
<li>a numerical recipe for computation of $Q(\lambda)$ over $D$ given $\lambda \in \mathbb{R}^m$.</li>
<li>sampling from the given distribution $p(x)$ given $\lambda$.</li>
</ol>
<p>For the first part, the code leverages <code>np.polynomial.legendre.leggauss</code> for Lengendre-Gauss integral, and <code>scipy.optimize.minimize</code> for Newton method.</p>
<pre data-lang="python" style="background-color:#383838;color:#e6e1dc;" class="language-python "><code class="language-python" data-lang="python"><span style="color:#f92672dd;">import </span><span>numpy </span><span style="color:#cc7833;">as </span><span>np
</span><span style="color:#f92672dd;">import </span><span>scipy
</span><span>
</span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">vandermonde_matrix</span><span>(</span><span style="font-style:italic;color:#fd971f;">points</span><span>, </span><span style="font-style:italic;color:#fd971f;">degree</span><span>, </span><span style="font-style:italic;color:#fd971f;">skip_first_col</span><span style="color:#cc7833;">=</span><span style="color:#6e9cbe;">False</span><span>):
</span><span>    points </span><span style="color:#cc7833;">= </span><span>points.reshape(</span><span style="color:#cc7833;">-</span><span style="color:#a5c261;">1</span><span>, </span><span style="color:#a5c261;">1</span><span>)
</span><span>    start_idx </span><span style="color:#cc7833;">= </span><span style="color:#a5c261;">1 </span><span style="color:#cc7833;">if </span><span>skip_first_col </span><span style="color:#cc7833;">else </span><span style="color:#a5c261;">0
</span><span>    powers </span><span style="color:#cc7833;">= </span><span>np.arange(start_idx, degree </span><span style="color:#cc7833;">+ </span><span style="color:#a5c261;">1</span><span>)
</span><span>    </span><span style="color:#cc7833;">return </span><span>np.power(points, powers)
</span><span>
</span><span>
</span><span style="font-style:italic;color:#cc7833;">class </span><span style="text-decoration:underline;color:#ffc66d;">MaxEntropyPDFSolver</span><span>(</span><span style="text-decoration:underline;font-style:italic;color:#ffc66d;">object</span><span>):
</span><span>    </span><span style="color:#95815e;">&quot;&quot;&quot;This class implements the solver for probability density function (PDF)
</span><span style="color:#95815e;">    from this paper:
</span><span style="color:#95815e;">    ENTROPY DENSITIES: WITH AN APPLICATION TO AUTOREGRESSIVE CONDITIONAL SKEWNESS AND KURTOSIS
</span><span style="color:#95815e;">
</span><span style="color:#95815e;">    &quot;&quot;&quot;
</span><span>
</span><span>    </span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#da4939;">__init__</span><span>(</span><span style="font-style:italic;color:#fd971f;">self</span><span>, </span><span style="font-style:italic;color:#fd971f;">moments</span><span>, </span><span style="font-style:italic;color:#fd971f;">bounds</span><span>, </span><span style="font-style:italic;color:#fd971f;">degree</span><span>, </span><span style="font-style:italic;color:#fd971f;">num_point</span><span style="color:#cc7833;">=</span><span style="color:#a5c261;">40</span><span>):
</span><span>        </span><span style="color:#d0d0ff;">self</span><span>.moments </span><span style="color:#cc7833;">= </span><span>moments
</span><span>        </span><span style="color:#d0d0ff;">self</span><span>.bounds </span><span style="color:#cc7833;">= </span><span>bounds
</span><span>        </span><span style="color:#d0d0ff;">self</span><span>.degree </span><span style="color:#cc7833;">= </span><span>degree
</span><span>        </span><span style="color:#d0d0ff;">self</span><span>.num_point </span><span style="color:#cc7833;">= </span><span>num_point
</span><span>
</span><span>    </span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">_get_quadrature</span><span>(</span><span style="font-style:italic;color:#fd971f;">self</span><span>, </span><span style="font-style:italic;color:#fd971f;">n</span><span>, </span><span style="font-style:italic;color:#fd971f;">degree</span><span>, </span><span style="font-style:italic;color:#fd971f;">bounds</span><span>):
</span><span>        low, high </span><span style="color:#cc7833;">= </span><span>bounds
</span><span>        z, w </span><span style="color:#cc7833;">= </span><span>np.polynomial.legendre.leggauss(n)
</span><span>        x </span><span style="color:#cc7833;">= </span><span>(high </span><span style="color:#cc7833;">- </span><span>low) </span><span style="color:#cc7833;">/ </span><span style="color:#a5c261;">2 </span><span style="color:#cc7833;">* </span><span>z </span><span style="color:#cc7833;">+ </span><span>(low </span><span style="color:#cc7833;">+ </span><span>high) </span><span style="color:#cc7833;">/ </span><span style="color:#a5c261;">2
</span><span>        x_mat </span><span style="color:#cc7833;">= </span><span>vandermonde_matrix(x, degree, </span><span style="font-style:italic;color:#fd971f;">skip_first_col</span><span style="color:#cc7833;">=</span><span style="color:#6e9cbe;">True</span><span>)
</span><span>        </span><span style="color:#cc7833;">return </span><span>x_mat, w
</span><span>
</span><span>    </span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">_make_objective</span><span>(</span><span style="font-style:italic;color:#fd971f;">self</span><span>, </span><span style="font-style:italic;color:#fd971f;">x_mat</span><span>, </span><span style="font-style:italic;color:#fd971f;">weights</span><span>, </span><span style="font-style:italic;color:#fd971f;">moments</span><span>):
</span><span>        x_pow </span><span style="color:#cc7833;">= </span><span>np.array(x_mat)
</span><span>        w </span><span style="color:#cc7833;">= </span><span>np.array(weights)
</span><span>        b </span><span style="color:#cc7833;">= </span><span>np.array(moments)
</span><span>
</span><span>        </span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">objective</span><span>(</span><span style="font-style:italic;color:#fd971f;">x</span><span>):
</span><span>            A </span><span style="color:#cc7833;">= </span><span>x_pow </span><span style="color:#cc7833;">- </span><span>b
</span><span>            y </span><span style="color:#cc7833;">= </span><span>np.dot(np.exp(A </span><span style="color:#cc7833;">@ </span><span>x), w)
</span><span>            </span><span style="color:#cc7833;">return </span><span>y
</span><span>
</span><span>        </span><span style="color:#cc7833;">return </span><span>objective
</span><span>
</span><span>    </span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">_make_jacobian</span><span>(</span><span style="font-style:italic;color:#fd971f;">self</span><span>, </span><span style="font-style:italic;color:#fd971f;">x_mat</span><span>, </span><span style="font-style:italic;color:#fd971f;">weights</span><span>, </span><span style="font-style:italic;color:#fd971f;">moments</span><span>):
</span><span>        x_pow </span><span style="color:#cc7833;">= </span><span>np.array(x_mat)
</span><span>        w </span><span style="color:#cc7833;">= </span><span>np.array(weights)
</span><span>        b </span><span style="color:#cc7833;">= </span><span>np.array(moments)
</span><span>
</span><span>        </span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">jacobian</span><span>(</span><span style="font-style:italic;color:#fd971f;">x</span><span>):
</span><span>            A </span><span style="color:#cc7833;">= </span><span>x_pow </span><span style="color:#cc7833;">- </span><span>b
</span><span>            grad </span><span style="color:#cc7833;">= </span><span>A.T </span><span style="color:#cc7833;">@ </span><span>(w </span><span style="color:#cc7833;">* </span><span>np.exp(A </span><span style="color:#cc7833;">@ </span><span>x))
</span><span>            </span><span style="color:#cc7833;">return </span><span>grad
</span><span>
</span><span>        </span><span style="color:#cc7833;">return </span><span>jacobian
</span><span>
</span><span>    </span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">solve</span><span>(</span><span style="font-style:italic;color:#fd971f;">self</span><span>):
</span><span>        quad_x, quad_w </span><span style="color:#cc7833;">= </span><span style="color:#d0d0ff;">self</span><span>._get_quadrature(</span><span style="color:#d0d0ff;">self</span><span>.num_point, </span><span style="color:#d0d0ff;">self</span><span>.degree, </span><span style="color:#d0d0ff;">self</span><span>.bounds)
</span><span>        obj </span><span style="color:#cc7833;">= </span><span style="color:#d0d0ff;">self</span><span>._make_objective(quad_x, quad_w, </span><span style="color:#d0d0ff;">self</span><span>.moments)
</span><span>        jac </span><span style="color:#cc7833;">= </span><span style="color:#d0d0ff;">self</span><span>._make_jacobian(quad_x, quad_w, </span><span style="color:#d0d0ff;">self</span><span>.moments)
</span><span>        x0 </span><span style="color:#cc7833;">= </span><span>np.zeros(</span><span style="color:#d0d0ff;">self</span><span>.degree)
</span><span>        result </span><span style="color:#cc7833;">= </span><span>scipy.optimize.minimize(obj, x0, </span><span style="font-style:italic;color:#fd971f;">jac</span><span style="color:#cc7833;">=</span><span>jac)
</span><span>        </span><span style="color:#cc7833;">return </span><span>result.x
</span></code></pre>
<p>For the second part, the gist is to use rejection sampling from the region $D \times h$, where $h = \max_{x \in D} p(x)$.
There are some missing details of converting centered moments $\int_D (x - m_1)^2 p(x) dx = m_i$ to uncentered moments $\int_D x^i p(x) dx = b_i$, in the construction of $p(x)$.</p>
<pre data-lang="python" style="background-color:#383838;color:#e6e1dc;" class="language-python "><code class="language-python" data-lang="python"><span style="color:#f92672dd;">import </span><span>numpy </span><span style="color:#cc7833;">as </span><span>np
</span><span style="color:#f92672dd;">import </span><span>scipy
</span><span>
</span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">find_max</span><span>(</span><span style="font-style:italic;color:#fd971f;">fn</span><span>, </span><span style="font-style:italic;color:#fd971f;">bounds</span><span>):
</span><span>    </span><span style="color:#95815e;">&quot;&quot;&quot;find the maximum of a scalar fn on a finite interval&quot;&quot;&quot;
</span><span>    res </span><span style="color:#cc7833;">= </span><span>scipy.optimize.minimize_scalar(
</span><span>        </span><span style="font-style:italic;color:#cc7833;">lambda </span><span style="font-style:italic;color:#fd971f;">x</span><span>: </span><span style="color:#cc7833;">-</span><span>fn(x), </span><span style="font-style:italic;color:#fd971f;">bounds</span><span style="color:#cc7833;">=</span><span>bounds, </span><span style="font-style:italic;color:#fd971f;">method</span><span style="color:#cc7833;">=</span><span style="color:#c1be91;">&quot;bounded&quot;
</span><span>    )
</span><span>    </span><span style="color:#cc7833;">return -</span><span>res.fun
</span><span>
</span><span>
</span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">integrate</span><span>(</span><span style="font-style:italic;color:#fd971f;">fn</span><span>, </span><span style="font-style:italic;color:#fd971f;">bounds</span><span>, </span><span style="font-style:italic;color:#fd971f;">n_points</span><span style="color:#cc7833;">=</span><span style="color:#a5c261;">100</span><span>):
</span><span>    low, high </span><span style="color:#cc7833;">= </span><span>bounds
</span><span>    width </span><span style="color:#cc7833;">= </span><span>(high </span><span style="color:#cc7833;">- </span><span>low) </span><span style="color:#cc7833;">/ </span><span>n_points
</span><span>    x </span><span style="color:#cc7833;">= </span><span>np.linspace(low, high, n_points)
</span><span>    answer </span><span style="color:#cc7833;">= </span><span>np.sum(fn(x)) </span><span style="color:#cc7833;">* </span><span>width
</span><span>    </span><span style="color:#cc7833;">return </span><span>answer
</span><span>
</span><span>
</span><span style="font-style:italic;color:#cc7833;">def </span><span style="color:#ffc66d;">sample</span><span>(</span><span style="font-style:italic;color:#fd971f;">pdf</span><span>, </span><span style="font-style:italic;color:#fd971f;">bounds</span><span>, </span><span style="font-style:italic;color:#fd971f;">n_sample</span><span>):
</span><span>    </span><span style="color:#95815e;">&quot;&quot;&quot;generates `n_sample` datapoint given the distribution&quot;&quot;&quot;
</span><span>    low, hi </span><span style="color:#cc7833;">= </span><span>bounds
</span><span>    pdf_max </span><span style="color:#cc7833;">= </span><span>find_max(pdf, bounds)
</span><span>    pdf_area </span><span style="color:#cc7833;">= </span><span>integrate(pdf, bounds)
</span><span>    acceptance_rate </span><span style="color:#cc7833;">= </span><span>pdf_area </span><span style="color:#cc7833;">/ </span><span>((hi </span><span style="color:#cc7833;">- </span><span>low) </span><span style="color:#cc7833;">* </span><span>pdf_max)
</span><span>    x_rv </span><span style="color:#cc7833;">= </span><span>scipy.stats.uniform(</span><span style="font-style:italic;color:#fd971f;">loc</span><span style="color:#cc7833;">=</span><span>low, </span><span style="font-style:italic;color:#fd971f;">scale</span><span style="color:#cc7833;">=</span><span>hi </span><span style="color:#cc7833;">- </span><span>low)
</span><span>    y_rv </span><span style="color:#cc7833;">= </span><span>scipy.stats.uniform(</span><span style="font-style:italic;color:#fd971f;">loc</span><span style="color:#cc7833;">=</span><span style="color:#a5c261;">0</span><span>, </span><span style="font-style:italic;color:#fd971f;">scale</span><span style="color:#cc7833;">=</span><span>pdf_max)
</span><span>    </span><span style="color:#da4939;">print</span><span>(</span><span style="color:#c1be91;">&quot;acceptance rate is &quot;</span><span>, acceptance_rate)
</span><span>
</span><span>    </span><span style="color:#95815e;"># sample in a loop;
</span><span>    result </span><span style="color:#cc7833;">= </span><span>np.zeros(n_sample)
</span><span>    needed, idx </span><span style="color:#cc7833;">= </span><span>n_sample, </span><span style="color:#a5c261;">0
</span><span>    </span><span style="color:#cc7833;">while </span><span>needed </span><span style="color:#cc7833;">&gt; </span><span style="color:#a5c261;">0</span><span>:
</span><span>        n_draw </span><span style="color:#cc7833;">= </span><span style="font-style:italic;color:#6e9cbe;">int</span><span>(</span><span style="color:#a5c261;">0.5 </span><span style="color:#cc7833;">* </span><span>needed </span><span style="color:#cc7833;">/ </span><span>acceptance_rate)
</span><span>        xs </span><span style="color:#cc7833;">= </span><span>x_rv.rvs(</span><span style="font-style:italic;color:#fd971f;">size</span><span style="color:#cc7833;">=</span><span>n_draw)
</span><span>        ys </span><span style="color:#cc7833;">= </span><span>y_rv.rvs(</span><span style="font-style:italic;color:#fd971f;">size</span><span style="color:#cc7833;">=</span><span>n_draw)
</span><span>        accept_idx </span><span style="color:#cc7833;">= </span><span>ys </span><span style="color:#cc7833;">&lt; </span><span>pdf(xs)
</span><span>        batch </span><span style="color:#cc7833;">= </span><span>xs[accept_idx]
</span><span>        buffer_size </span><span style="color:#cc7833;">= </span><span style="color:#da4939;">min</span><span>(</span><span style="color:#da4939;">len</span><span>(batch), needed)
</span><span>        result[idx : idx </span><span style="color:#cc7833;">+ </span><span>buffer_size] </span><span style="color:#cc7833;">= </span><span>batch[:buffer_size]
</span><span>        idx </span><span style="color:#cc7833;">= </span><span>idx </span><span style="color:#cc7833;">+ </span><span>buffer_size
</span><span>        needed </span><span style="color:#cc7833;">= </span><span>needed </span><span style="color:#cc7833;">- </span><span>buffer_size
</span><span>    </span><span style="color:#cc7833;">return </span><span>result
</span></code></pre>

</div>

        </div>
        <!-- <footer>
            ©2017-2024 — <a class="white" href="https://vincentprouillet.com">Vincent Prouillet</a> and <a class="white" href="https://github.com/getzola/zola/graphs/contributors">contributors</a>
        </footer> -->

    </body>
</html>
